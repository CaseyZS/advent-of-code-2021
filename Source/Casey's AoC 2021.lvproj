﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="21008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Day 01" Type="Folder">
			<Item Name="Day 01.vi" Type="VI" URL="../Day 01/Day 01.vi"/>
		</Item>
		<Item Name="Day 02" Type="Folder">
			<Item Name="Day 02.vi" Type="VI" URL="../Day 02/Day 02.vi"/>
		</Item>
		<Item Name="Day 03" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 03 - Eliminate by Bit.vi" Type="VI" URL="../Day 03/Day 03 - Eliminate by Bit.vi"/>
				<Item Name="Day 03 - Get Most Common Values.vi" Type="VI" URL="../Day 03/Day 03 - Get Most Common Values.vi"/>
			</Item>
			<Item Name="Day 03.vi" Type="VI" URL="../Day 03/Day 03.vi"/>
		</Item>
		<Item Name="Day 04" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 04 - Check Bingo.vi" Type="VI" URL="../Day 04/Day 04 - Check Bingo.vi"/>
				<Item Name="Day 04 - Update Board.vi" Type="VI" URL="../Day 04/Day 04 - Update Board.vi"/>
			</Item>
			<Item Name="Day 04.vi" Type="VI" URL="../Day 04/Day 04.vi"/>
		</Item>
		<Item Name="Day 05" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 05 - Increment Lines.vi" Type="VI" URL="../Day 05/Day 05 - Increment Lines.vi"/>
			</Item>
			<Item Name="Day 05.vi" Type="VI" URL="../Day 05/Day 05.vi"/>
		</Item>
		<Item Name="Day 06" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 06 - Reproduce (old).vi" Type="VI" URL="../Day 06/Day 06 - Reproduce (old).vi"/>
				<Item Name="Day 06 - Reproduce.vi" Type="VI" URL="../Day 06/Day 06 - Reproduce.vi"/>
			</Item>
			<Item Name="Day 06.vi" Type="VI" URL="../Day 06/Day 06.vi"/>
		</Item>
		<Item Name="Day 07" Type="Folder">
			<Item Name="Day 07.vi" Type="VI" URL="../Day 07/Day 07.vi"/>
		</Item>
		<Item Name="Day 08" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 08 - Deduce from Segment Frequency.vi" Type="VI" URL="../Day 08/Day 08 - Deduce from Segment Frequency.vi"/>
				<Item Name="Day 08 - Deduce from Segments in Numbers - Part 2.vi" Type="VI" URL="../Day 08/Day 08 - Deduce from Segments in Numbers - Part 2.vi"/>
				<Item Name="Day 08 - Deduce from Segments in Numbers.vi" Type="VI" URL="../Day 08/Day 08 - Deduce from Segments in Numbers.vi"/>
				<Item Name="Day 08 - Deduce Numbers.vi" Type="VI" URL="../Day 08/Day 08 - Deduce Numbers.vi"/>
				<Item Name="Day 08 - Index to Letter.vi" Type="VI" URL="../Day 08/Day 08 - Index to Letter.vi"/>
				<Item Name="Day 08 - Letter to Index.vi" Type="VI" URL="../Day 08/Day 08 - Letter to Index.vi"/>
				<Item Name="Day 08 - Translate to Numbers.vi" Type="VI" URL="../Day 08/Day 08 - Translate to Numbers.vi"/>
			</Item>
			<Item Name="Day 08 - Part 1.vi" Type="VI" URL="../Day 08/Day 08 - Part 1.vi"/>
			<Item Name="Day 08 - Part 2.vi" Type="VI" URL="../Day 08/Day 08 - Part 2.vi"/>
		</Item>
		<Item Name="Day 09" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 09 - Expand Basin.vi" Type="VI" URL="../Day 09/Day 09 - Expand Basin.vi"/>
				<Item Name="Day 09 - Get Neighbors.vi" Type="VI" URL="../Day 09/Day 09 - Get Neighbors.vi"/>
				<Item Name="Day 09 - Shift Arrays.vi" Type="VI" URL="../Day 09/Day 09 - Shift Arrays.vi"/>
			</Item>
			<Item Name="Day 09.vi" Type="VI" URL="../Day 09/Day 09.vi"/>
		</Item>
		<Item Name="Day 10" Type="Folder">
			<Item Name="Day 10 - Part 1.vi" Type="VI" URL="../Day 10/Day 10 - Part 1.vi"/>
			<Item Name="Day 10 - Part 2.vi" Type="VI" URL="../Day 10/Day 10 - Part 2.vi"/>
			<Item Name="Day 10.vi" Type="VI" URL="../Day 10/Day 10.vi"/>
		</Item>
		<Item Name="Day 11" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 11 - Chain Flashes.vi" Type="VI" URL="../Day 11/Day 11 - Chain Flashes.vi"/>
				<Item Name="Day 11 - Manipulate Trim.vi" Type="VI" URL="../Day 11/Day 11 - Manipulate Trim.vi"/>
			</Item>
			<Item Name="Day 11.vi" Type="VI" URL="../Day 11/Day 11.vi"/>
		</Item>
		<Item Name="Day 12" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 12 - Add Small Cave.vi" Type="VI" URL="../Day 12/Day 12 - Add Small Cave.vi"/>
				<Item Name="Day 12 - Cave Path Parameters Clust.ctl" Type="VI" URL="../Day 12/Day 12 - Cave Path Parameters Clust.ctl"/>
				<Item Name="Day 12 - Get Next Move - Part 2.vi" Type="VI" URL="../Day 12/Day 12 - Get Next Move - Part 2.vi"/>
				<Item Name="Day 12 - Get Next Move.vi" Type="VI" URL="../Day 12/Day 12 - Get Next Move.vi"/>
				<Item Name="Day 12 - Move to Cave - Part 2.vi" Type="VI" URL="../Day 12/Day 12 - Move to Cave - Part 2.vi"/>
				<Item Name="Day 12 - Move to Cave.vi" Type="VI" URL="../Day 12/Day 12 - Move to Cave.vi"/>
				<Item Name="Day 12 - Remove Small Cave.vi" Type="VI" URL="../Day 12/Day 12 - Remove Small Cave.vi"/>
			</Item>
			<Item Name="Day 12.vi" Type="VI" URL="../Day 12/Day 12.vi"/>
		</Item>
		<Item Name="Day 13" Type="Folder">
			<Item Name="Day 13.vi" Type="VI" URL="../Day 13/Day 13.vi"/>
		</Item>
		<Item Name="Day 14" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 14 - Non-Recurse.vi" Type="VI" URL="../Day 14/Day 14 - Non-Recurse.vi"/>
				<Item Name="Day 14 - Recurse.vi" Type="VI" URL="../Day 14/Day 14 - Recurse.vi"/>
			</Item>
			<Item Name="Day 14 - Part 1.vi" Type="VI" URL="../Day 14/Day 14 - Part 1.vi"/>
			<Item Name="Day 14 - Part 2.vi" Type="VI" URL="../Day 14/Day 14 - Part 2.vi"/>
		</Item>
		<Item Name="Day 15" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="SubVIs" Type="Folder">
				<Item Name="DFS" Type="Folder">
					<Item Name="Day 15 (Recursive).vi" Type="VI" URL="../Day 15/Day 15 (Recursive).vi"/>
					<Item Name="Day 15 - Consolidate Risk.vi" Type="VI" URL="../Day 15/Day 15 - Consolidate Risk.vi"/>
					<Item Name="Day 15 - Create Sub-Maps.vi" Type="VI" URL="../Day 15/Day 15 - Create Sub-Maps.vi"/>
					<Item Name="Day 15 - Filter Moves.vi" Type="VI" URL="../Day 15/Day 15 - Filter Moves.vi"/>
					<Item Name="Day 15 - Get Potential Moves.vi" Type="VI" URL="../Day 15/Day 15 - Get Potential Moves.vi"/>
					<Item Name="Day 15 - Move (Recursive).vi" Type="VI" URL="../Day 15/Day 15 - Move (Recursive).vi"/>
					<Item Name="Day 15 - Reset for Sub-Map.vi" Type="VI" URL="../Day 15/Day 15 - Reset for Sub-Map.vi"/>
					<Item Name="Day 15 - Routing Parameters Clust.ctl" Type="VI" URL="../Day 15/Day 15 - Routing Parameters Clust.ctl"/>
				</Item>
				<Item Name="Dijkstra" Type="Folder">
					<Item Name="Day 15 - Dijkstra Parameters Clust.ctl" Type="VI" URL="../Day 15/Day 15 - Dijkstra Parameters Clust.ctl"/>
					<Item Name="Day 15 - Get Lowest Risk Coords.vi" Type="VI" URL="../Day 15/Day 15 - Get Lowest Risk Coords.vi"/>
					<Item Name="Day 15 - Update Risk Evaluations.vi" Type="VI" URL="../Day 15/Day 15 - Update Risk Evaluations.vi"/>
				</Item>
				<Item Name="Day 15 - Initialize Routing Parameters - Dijkstra.vi" Type="VI" URL="../Day 15/Day 15 - Initialize Routing Parameters - Dijkstra.vi"/>
				<Item Name="Day 15 - Initialize Routing Parameters - Part 1.vi" Type="VI" URL="../Day 15/Day 15 - Initialize Routing Parameters - Part 1.vi"/>
				<Item Name="Day 15 - Initialize Routing Parameters - Part 2.vi" Type="VI" URL="../Day 15/Day 15 - Initialize Routing Parameters - Part 2.vi"/>
				<Item Name="Day 15 - Parse Input.vi" Type="VI" URL="../Day 15/Day 15 - Parse Input.vi"/>
			</Item>
			<Item Name="Day 15 - Part 1.vi" Type="VI" URL="../Day 15/Day 15 - Part 1.vi"/>
			<Item Name="Day 15 - Part 2.vi" Type="VI" URL="../Day 15/Day 15 - Part 2.vi"/>
		</Item>
		<Item Name="Day 16" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 16 - Parse Input.vi" Type="VI" URL="../Day 16/Day 16 - Parse Input.vi"/>
				<Item Name="Day 16 - Parse Packet.vi" Type="VI" URL="../Day 16/Day 16 - Parse Packet.vi"/>
			</Item>
			<Item Name="Day 16.vi" Type="VI" URL="../Day 16/Day 16.vi"/>
		</Item>
		<Item Name="Day 17" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 17 - Calculate X Position.vi" Type="VI" URL="../Day 17/Day 17 - Calculate X Position.vi"/>
				<Item Name="Day 17 - Calculate Y Position.vi" Type="VI" URL="../Day 17/Day 17 - Calculate Y Position.vi"/>
				<Item Name="Day 17 - Discover All Y Velocities.vi" Type="VI" URL="../Day 17/Day 17 - Discover All Y Velocities.vi"/>
				<Item Name="Day 17 - Discover Max and Min X Speeds.vi" Type="VI" URL="../Day 17/Day 17 - Discover Max and Min X Speeds.vi"/>
				<Item Name="Day 17 - Discover Max Y Velocity and Steps.vi" Type="VI" URL="../Day 17/Day 17 - Discover Max Y Velocity and Steps.vi"/>
				<Item Name="Day 17 - Generate X Map.vi" Type="VI" URL="../Day 17/Day 17 - Generate X Map.vi"/>
				<Item Name="Day 17 - Generate Y Map.vi" Type="VI" URL="../Day 17/Day 17 - Generate Y Map.vi"/>
				<Item Name="Day 17 - Parse Part 2 Example.vi" Type="VI" URL="../Day 17/Day 17 - Parse Part 2 Example.vi"/>
			</Item>
			<Item Name="Day 17.vi" Type="VI" URL="../Day 17/Day 17.vi"/>
		</Item>
		<Item Name="Day 18" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 18 - Add Numbers.vi" Type="VI" URL="../Day 18/Day 18 - Add Numbers.vi"/>
				<Item Name="Day 18 - Explode Check.vi" Type="VI" URL="../Day 18/Day 18 - Explode Check.vi"/>
				<Item Name="Day 18 - Explode.vi" Type="VI" URL="../Day 18/Day 18 - Explode.vi"/>
				<Item Name="Day 18 - Magnitude.vi" Type="VI" URL="../Day 18/Day 18 - Magnitude.vi"/>
				<Item Name="Day 18 - Parse Input.vi" Type="VI" URL="../Day 18/Day 18 - Parse Input.vi"/>
				<Item Name="Day 18 - Split.vi" Type="VI" URL="../Day 18/Day 18 - Split.vi"/>
			</Item>
			<Item Name="Day 18.vi" Type="VI" URL="../Day 18/Day 18.vi"/>
		</Item>
		<Item Name="Day 19" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 19 - Find Global Positions.vi" Type="VI" URL="../Day 19/Day 19 - Find Global Positions.vi"/>
				<Item Name="Day 19 - Get Sensor Rotations.vi" Type="VI" URL="../Day 19/Day 19 - Get Sensor Rotations.vi"/>
				<Item Name="Day 19 - Parse Input.vi" Type="VI" URL="../Day 19/Day 19 - Parse Input.vi"/>
				<Item Name="Rotations.vi" Type="VI" URL="../Day 19/Rotations.vi"/>
			</Item>
			<Item Name="Day 19 - Part 1.vi" Type="VI" URL="../Day 19/Day 19 - Part 1.vi"/>
			<Item Name="Day 19 - Part 2.vi" Type="VI" URL="../Day 19/Day 19 - Part 2.vi"/>
		</Item>
		<Item Name="Day 20" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 20 - Add Padding.vi" Type="VI" URL="../Day 20/Day 20 - Add Padding.vi"/>
				<Item Name="Day 20 - Enhance.vi" Type="VI" URL="../Day 20/Day 20 - Enhance.vi"/>
				<Item Name="Day 20 - Parse Input - Image Enhancement Algorithm.vi" Type="VI" URL="../Day 20/Day 20 - Parse Input - Image Enhancement Algorithm.vi"/>
				<Item Name="Day 20 - Parse Input - Image.vi" Type="VI" URL="../Day 20/Day 20 - Parse Input - Image.vi"/>
			</Item>
			<Item Name="Day 20.vi" Type="VI" URL="../Day 20/Day 20.vi"/>
		</Item>
		<Item Name="Day 21" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 21 - Update Instances and Scores.vi" Type="VI" URL="../Day 21/Day 21 - Update Instances and Scores.vi"/>
			</Item>
			<Item Name="Day 21 - Part 1.vi" Type="VI" URL="../Day 21/Day 21 - Part 1.vi"/>
			<Item Name="Day 21 - Part 2.vi" Type="VI" URL="../Day 21/Day 21 - Part 2.vi"/>
		</Item>
		<Item Name="Day 22" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 22 - Add Latest Cube.vi" Type="VI" URL="../Day 22/Day 22 - Add Latest Cube.vi"/>
				<Item Name="Day 22 - Create Decomposed Cubes.vi" Type="VI" URL="../Day 22/Day 22 - Create Decomposed Cubes.vi"/>
				<Item Name="Day 22 - Create Decomposed Segments.vi" Type="VI" URL="../Day 22/Day 22 - Create Decomposed Segments.vi"/>
				<Item Name="Day 22 - Cube Dimensions Clust.ctl" Type="VI" URL="../Day 22/Day 22 - Cube Dimensions Clust.ctl"/>
				<Item Name="Day 22 - Decompose Cubes.vi" Type="VI" URL="../Day 22/Day 22 - Decompose Cubes.vi"/>
				<Item Name="Day 22 - Decomposed Cube On_Off.vi" Type="VI" URL="../Day 22/Day 22 - Decomposed Cube On_Off.vi"/>
				<Item Name="Day 22 - Encompass Check.vi" Type="VI" URL="../Day 22/Day 22 - Encompass Check.vi"/>
				<Item Name="Day 22 - Encompassed By Enum.ctl" Type="VI" URL="../Day 22/Day 22 - Encompassed By Enum.ctl"/>
				<Item Name="Day 22 - Find Cube Intersection.vi" Type="VI" URL="../Day 22/Day 22 - Find Cube Intersection.vi"/>
				<Item Name="Day 22 - Nullify Cube Intersection.vi" Type="VI" URL="../Day 22/Day 22 - Nullify Cube Intersection.vi"/>
				<Item Name="Day 22 - Overlap Check.vi" Type="VI" URL="../Day 22/Day 22 - Overlap Check.vi"/>
				<Item Name="Day 22 - Parse Input.vi" Type="VI" URL="../Day 22/Day 22 - Parse Input.vi"/>
			</Item>
			<Item Name="Day 22 - Part 1.vi" Type="VI" URL="../Day 22/Day 22 - Part 1.vi"/>
			<Item Name="Day 22 - Part 2 (Intersection Method).vi" Type="VI" URL="../Day 22/Day 22 - Part 2 (Intersection Method).vi"/>
			<Item Name="Day 22 - Part 2.vi" Type="VI" URL="../Day 22/Day 22 - Part 2.vi"/>
		</Item>
		<Item Name="Day 23" Type="Folder">
			<Item Name="SubVIs" Type="Folder"/>
			<Item Name="Day 23.vi" Type="VI" URL="../Day 23/Day 23.vi"/>
		</Item>
		<Item Name="Day 24" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 24 - Convert Number to Digit Array.vi" Type="VI" URL="../Day 24/Day 24 - Convert Number to Digit Array.vi"/>
				<Item Name="Day 24 - Dataflow Execution.vi" Type="VI" URL="../Day 24/Day 24 - Dataflow Execution.vi"/>
				<Item Name="Day 24 - Execute Instructions.vi" Type="VI" URL="../Day 24/Day 24 - Execute Instructions.vi"/>
				<Item Name="Day 24 - Get Next Number.vi" Type="VI" URL="../Day 24/Day 24 - Get Next Number.vi"/>
				<Item Name="Day 24 - Integer Division.vi" Type="VI" URL="../Day 24/Day 24 - Integer Division.vi"/>
				<Item Name="Day 24 - Parse Input (Data Flow).vi" Type="VI" URL="../Day 24/Day 24 - Parse Input (Data Flow).vi"/>
				<Item Name="Day 24 - Parse Input.vi" Type="VI" URL="../Day 24/Day 24 - Parse Input.vi"/>
				<Item Name="Day 24 - Restore.vi" Type="VI" URL="../Day 24/Day 24 - Restore.vi"/>
				<Item Name="Day 24 - Reverse Engineer Z and W.vi" Type="VI" URL="../Day 24/Day 24 - Reverse Engineer Z and W.vi"/>
				<Item Name="Day 24 - Variables.vi" Type="VI" URL="../Day 24/Day 24 - Variables.vi"/>
			</Item>
			<Item Name="Day 24 - Reverse Engineer.vi" Type="VI" URL="../Day 24/Day 24 - Reverse Engineer.vi"/>
			<Item Name="Day 24.vi" Type="VI" URL="../Day 24/Day 24.vi"/>
		</Item>
		<Item Name="Day 25" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Day 25 - Parse Input.vi" Type="VI" URL="../Day 25/Day 25 - Parse Input.vi"/>
			</Item>
			<Item Name="Day 25.vi" Type="VI" URL="../Day 25/Day 25.vi"/>
		</Item>
		<Item Name="General SubVIs" Type="Folder">
			<Item Name="Create Permutations.vi" Type="VI" URL="../SubVIs/Create Permutations.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Convert Array to Set__vipm_lv_collection_ext.vim" Type="VI" URL="/&lt;vilib&gt;/VIPM/Collection_Extensions/Set/Convert Array to Set__vipm_lv_collection_ext.vim"/>
				<Item Name="Create Empty Set__vipm_lv_collection_ext.vim" Type="VI" URL="/&lt;vilib&gt;/VIPM/Collection_Extensions/Set/Create Empty Set__vipm_lv_collection_ext.vim"/>
				<Item Name="Less Comparable.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Less/Less Comparable/Less Comparable.lvclass"/>
				<Item Name="Less Functor.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Less/Less Functor/Less Functor.lvclass"/>
				<Item Name="Less.vim" Type="VI" URL="/&lt;vilib&gt;/Comparison/Less.vim"/>
				<Item Name="LVMapReplaceAction.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMapReplaceAction.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_Matrix.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/Matrix/NI_Matrix.lvlib"/>
				<Item Name="Set Difference.vim" Type="VI" URL="/&lt;vilib&gt;/set operations/Set Difference.vim"/>
				<Item Name="Set Intersection.vim" Type="VI" URL="/&lt;vilib&gt;/set operations/Set Intersection.vim"/>
				<Item Name="Set Union.vim" Type="VI" URL="/&lt;vilib&gt;/set operations/Set Union.vim"/>
				<Item Name="Sort 1D Array Core.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Helpers/Sort 1D Array Core.vim"/>
				<Item Name="Sort 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Sort 1D Array.vim"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
